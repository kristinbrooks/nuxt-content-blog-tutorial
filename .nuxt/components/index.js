export { default as PrevNext } from '../../components/PrevNext.vue'

export const LazyPrevNext = import('../../components/PrevNext.vue' /* webpackChunkName: "components/PrevNext" */).then(c => c.default || c)
